import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taqqat_user_app_demo/widgets/common_material_button.dart';
import 'package:taqqat_user_app_demo/widgets/common_text_button.dart';
import 'package:taqqat_user_app_demo/constants.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.grey,
            size: 20,
          ),
          onPressed: () {},
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 15.0, bottom: 15),
              child: Text(
                'Welcome Back',
                style: kTitleTextStyle,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 15.0),
              child: Text(
                'Please sign in to continue',
                style: kDescriptionTextStyle,
              ),
            ),
            Row(
              children: [
                // CupertinoPicker(itemExtent: itemExtent, onSelectedItemChanged: onSelectedItemChanged, children: children)
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 50.0),
                    child: TextField(
                      cursorHeight: 30,
                      cursorColor: Colors.grey,
                      autofocus: true,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        labelText: 'Phone Number*',
                        labelStyle: kDescriptionTextStyle,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Center(
              child: Padding(
                padding: EdgeInsets.only(top: 20),
                child: CommonMaterialButton(
                    onPress: () {},
                    title: 'Next',
                    colour: Colors.teal,
                    fontColour: Colors.white),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Don\'t have an account?',
                  style: kTitleTextStyle.copyWith(
                      fontSize: 13, color: Colors.grey[700]),
                ),
                CommonTextButton(
                    buttonTitle: ' Create Account ',
                    textDecoration: TextStyle(
                      fontSize: 12,
                      color: Colors.teal,
                    ),
                    function: () {}),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
