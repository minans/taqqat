import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taqqat_user_app_demo/widgets/common_material_button.dart';
import 'package:taqqat_user_app_demo/widgets/common_text_button.dart';
import 'package:taqqat_user_app_demo/constants.dart';

class RegistrationScreen extends StatefulWidget {
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  bool isChecked = false;
   String? email;
  String? name;
   String? phoneNumber;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.grey,
            size: 20,
          ),
          onPressed: () {},
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Create Account',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 20,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15),
              child: Text('Please sign up to continue ',
                  style: kDescriptionTextStyle),
            ),
            Flexible(
              child: Padding(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Form(
                  key: _nameKey,     autovalidateMode: AutovalidateMode.onUserInteraction,
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter valid name';
                      }
                      if(validateName(value)==true){
                        return 'please enter valid name';
                      }
                      return null;
                    },
                    onChanged: (value){if(_nameKey.currentState!.validate()){
                      name=value;
                    }},
                    autofocus: false,
                    cursorColor: Colors.grey,
                    keyboardType: TextInputType.name,
                    decoration: InputDecoration(
                      labelText: 'Name',
                      labelStyle: kDescriptionTextStyle,
                      // border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Flexible(
              child: Padding(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Form(
                  key: _emailKey,     autovalidateMode: AutovalidateMode.onUserInteraction,
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter a valid email';
                      }
                      if(validateEmail(value)==true){return 'enter valid mail';}
                      return null;
                    },
                    onChanged: (value){if(_emailKey.currentState!.validate()){
                      email=value;
                    }},
                    autofocus: false,
                    cursorColor: Colors.grey,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      labelText: 'Email',
                      labelStyle: kDescriptionTextStyle,
                      // border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Flexible(
              child: Padding(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                child: Form(
                  key: _phoneKey,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter valid phone number';
                      }
if(validatePhone(value)==true){return 'enter valid number';}
                      return null;
                    },
                    onChanged: (value){if(_phoneKey.currentState!.validate()){
phoneNumber=value;
                    }},
                    autofocus: false,
                    cursorColor: Colors.grey,
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                      labelText: 'PhoneNumber',
                      labelStyle: kDescriptionTextStyle,
                      // border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15.0),
              child: Row(
                children: [
                  Checkbox(
                    side: BorderSide(style: BorderStyle.solid),
                    value: isChecked,
                    onChanged: (bool? value) {
                      setState(() {
                        isChecked = value!;
                      });
                    },
                  ),
                  Text(
                    'I accept the',
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.black,
                    ),
                  ),
                  CommonTextButton(
                    textDecoration: kDescriptionTextStyle.copyWith(
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                    ),
                    buttonTitle: 'terms and conditions',
                    function: () {},
                  ),
                ],
              ),
            ),
            Center(
              child: Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: CommonMaterialButton(
                    onPress: () {},
                    title: 'Next',
                    colour: Colors.teal,
                    fontColour: Colors.white),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Already have an account?',
                  style: kTitleTextStyle.copyWith(
                      fontSize: 13, color: Colors.grey[700]),
                ),
                CommonTextButton(
                    buttonTitle: 'login',
                    textDecoration: kDescriptionTextStyle.copyWith(
                        color: Colors.teal, fontWeight: FontWeight.w800),
                    function: () {})
              ],
            ),
          ],
        ),
      ),
    );
  }
}

final _nameKey = GlobalKey<FormState>();
final _emailKey = GlobalKey<FormState>();
final _phoneKey = GlobalKey<FormState>();
