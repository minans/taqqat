import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:taqqat_user_app_demo/widgets/common_material_button.dart';
import 'package:taqqat_user_app_demo/widgets/common_text_button.dart';
import 'package:taqqat_user_app_demo/constants.dart';

class OtpScreen extends StatefulWidget {
  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 1,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
            size: 20,
          ),
          onPressed: () {},
        ),
        title: Text(
          'Verification',
        ),
        titleSpacing: 90,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Center(
              child: Text(
            'A verification code has been sent to \n +966-9897 8787 20',
            style: kDescriptionTextStyle.copyWith(fontSize: 15),
            textAlign: TextAlign.center,
          )),
          Text(
            'Type your verification code to continue',
            style: kDescriptionTextStyle.copyWith(
                color: Colors.black, fontSize: 15),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              NewOtpWidget(),
              SizedBox(
                width: 20,
              ),
              NewOtpWidget(),
              SizedBox(
                width: 20,
              ),
              NewOtpWidget(),
              SizedBox(
                width: 20,
              ),
              NewOtpWidget()
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'OTP will expire in  ',
                style: kDescriptionTextStyle,
              ),
              Text(
                '0:20s',
                style: kDescriptionTextStyle.copyWith(color: Colors.black),
              )
            ],
          ),
          CommonMaterialButton(
              onPress: () {},
              title: 'VERIFY',
              colour: Colors.teal,
              fontColour: Colors.white),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Did’t get the code?',
                style: kDescriptionTextStyle.copyWith(color: Colors.black),
              ),
              CommonTextButton(
                buttonTitle: 'Resend',
                textDecoration: kDescriptionTextStyle,
                function: () {},
              )
            ],
          )
        ],
      ),
    );
  }
}

class NewOtpWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: 60,
      decoration: BoxDecoration(
        color: Colors.grey.shade300, // red as border color
      ),
      child: TextField(
        maxLength: 1,
        keyboardType: TextInputType.number,
        autofocus: true,
        cursorColor: Colors.grey,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
          border: InputBorder.none,
        ),
      ),
    );
  }
}
