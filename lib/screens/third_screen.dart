import 'package:flutter/material.dart';
import 'package:taqqat_user_app_demo/constants.dart';
import 'package:taqqat_user_app_demo/route.dart';

class ThirdPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizedBox cardFunction({
      required String printStatement,
      required String imageName,
      required String cardHeading,
      required String cardData,
    }) {
      return SizedBox(
        height:90,
        child: OutlinedButton(
          onPressed: () {
            print('$printStatement');
            Navigator.of(context).pushNamed(fourthRoute);
          },
          style: ButtonStyle(
              shape: MaterialStateProperty.all(RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0)))),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Image.asset(
                '$imageName',
                height: 50,
                width: 50,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '$cardHeading',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.black,
                      // fontSize: 15,
                    ),
                  ),
                  SizedBox(
                    //height: 50,
                    width: 200,
                    child: Text(
                      '$cardData ',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        //fontSize: 14,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
              Icon(
                Icons.arrow_forward_ios_rounded,
                //size: 20,
                color: Colors.grey,
              ),
            ],
          ),
        ),
      );
    }
    return Scaffold(
      body: SafeArea(
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/Survey-start-img – 3.png'),fit: BoxFit.fill,
                )

            ),
            child:Column(
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                    onPressed: () {
                      print('clicked');
                      Navigator.of(context).pushNamed(secondRoute);
                    },
                    icon: Icon(Icons.arrow_back_ios),
                    color: Colors.grey[700],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Let’s have a quick survey to know about solar power',
                    style:kTitleTextStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Lorem ipsum dolor sit ametsndi efio wjoedoe , consectetur adipiscing elit,',
                    style: kDescriptionTextStyle,
                  ),
                ),
                //Container(
                // height: deviceHeight*.6,
                //width: deviceWidth*.4,
                //decoration: BoxDecoration(
                //image: DecorationImage(
                Align(
                  alignment: Alignment.topRight,
                  child: Image.asset('images/Group 16225.png'),
                ),
                // ),
                //),
                // ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: cardFunction(
                    printStatement: 'next page',
                    imageName: 'images/home.png',
                    cardData: 'sndi efio doeenc kjoeddfddddqqqq',
                    cardHeading: 'Individual',
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: cardFunction(
                    printStatement: 'next page',
                    imageName: 'images/Component 15 – 1.png',
                    cardData: 'sndi efio wjoedoe cnijijsndi efio wjoedoe  sjkjdionc',
                    cardHeading: 'Business Owner',
                  ),
                ),
              ],
            ),
          ),
      ), );
  }
}
