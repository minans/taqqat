import 'package:flutter/material.dart';
import 'package:taqqat_user_app_demo/constants.dart';
import 'package:taqqat_user_app_demo/route.dart';
import 'package:taqqat_user_app_demo/widgets/base_row.dart';
import 'package:taqqat_user_app_demo/widgets/common_material_button.dart';

int valueNow = 500;
class FifthPage extends StatefulWidget {


  @override
  _FifthPageState createState() => _FifthPageState();
}

class _FifthPageState extends State<FifthPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/Survey-start-img – 3.png'),
              fit: BoxFit.fill,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                    onPressed: () {
                      print('previous page');
                      Navigator.of(context).pushNamed(fourthRoute);
                    },
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.grey[600],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Add your monthly Electricity Bill',
                  style: kTitleTextStyle,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Lorem ipsum r sit amet, consectetur adipiscing elitr sit amet, '
                  'consectetur adipiscing elitdolor sit amet, consectetur adipiscing elit,',
                  textAlign: TextAlign.start,
                  style: kDescriptionTextStyle,
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Image.asset(
                  'images/Group 16232.png',
                  height: 200,
                ),
              ),
              Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('Monthly Bill'),
                  )),
              SliderTheme(
                data: SliderThemeData(
                  trackHeight: 10,
                  thumbColor: Colors.white,
                  activeTrackColor: Colors.yellow[600],
                  inactiveTrackColor: Colors.grey[50],
                  thumbShape: RoundSliderThumbShape(enabledThumbRadius: 15),
                  //overlayColor: Colors.grey[50],
                ),
                child: Slider(
                  value: valueNow.toDouble(),
                  min: 100.0,
                  max: 1000.0,
                  divisions: 50,
                  onChanged: (double value) {
                    setState(() {
                      valueNow = value.toInt();
                    });
                  },
                ),
              ),
              Text(
                '$valueNow SAR',
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  shadowColor: Colors.grey,
                  child: Text(
                    '$valueNow SAR',
                  ),
                ),
              ),
              baseRow(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  CommonMaterialButton(
                    onPress: () {
                      print('previous page');
                      Navigator.of(context).pushNamed(fourthRoute);
                    },
                    title: 'Back',
                    colour: Colors.white,
                    fontColour: Colors.teal,
                  ),
                  CommonMaterialButton(
                    onPress: () {
                      Navigator.of(context).pushNamed(sixthRoute);
                    },
                    title: 'Next',
                    colour: Colors.teal,
                    fontColour: Colors.white,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
