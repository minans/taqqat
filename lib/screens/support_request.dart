import 'package:flutter/material.dart';
import 'package:taqqat_user_app_demo/constants.dart';
import 'chat_screen.dart';

class SupportRequestClass extends StatefulWidget {
  @override
  _SupportRequestClassState createState() => _SupportRequestClassState();
}

class _SupportRequestClassState extends State<SupportRequestClass> {
  Widget showChatScreen(BuildContext context) {
    return ChatScreen();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.chat_bubble,
            color: Colors.white,
          ),
          onPressed: () {
            showModalBottomSheet(
              context: context,
              builder: showChatScreen,
              isScrollControlled: true,
            );
          }),
      appBar: AppBar(
        elevation: .5,
        leading: Icon(Icons.arrow_back_ios),
        title: Text(
          'Support Request',
          textAlign: TextAlign.center,
          style: kHeadingTextStyle,
        ),
        titleSpacing: 60,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 10.0),
                      child: Text(
                        'TAQ-R-1001',
                        style: TextStyle(color: Colors.blueGrey),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.0),
                      child: Text(
                        'Solar panel remove',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 10.0),
                      child: Row(
                        children: [
                          Icon(
                            Icons.check_circle,
                            color: Colors.teal,
                            size: 13,
                          ),
                          Text(
                            'Resolved',
                            style: TextStyle(
                              color: Colors.teal,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.0),
                      child: Text(
                        '23 Sept 2020, 01:00 pm',
                        style: TextStyle(color: Colors.blueGrey),
                      ),
                    ),
                  ],
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.0),
              child: Text(
                'Details',
                style: TextStyle(fontSize: 15),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20, bottom: 20.0),
              child: Text(
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
                'Ut euismod eros ac velit rhoncus, eget pharetra nibh varius. Sed tortor justo, hendrerit eu blandit'
                ' eget, porta id urna. Quisque',
                style: kDescriptionTextStyle.copyWith(
                    color: Colors.blueGrey, fontSize: 12),
              ),
            ),
            Center(
              child: Image.asset('images/Mask Group 6.png'),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.0),
              child: Text('Please rate our service?'),
            ),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                  child: SizedBox(
                    width: 300,
                    child: Text(
                      'It is a long established fact that a reader will be distribution'
                      'distracted by the readable content of a page when normal',
                      style: kDescriptionTextStyle.copyWith(
                          color: Colors.blueGrey, fontSize: 12),
                    ),
                  ),
                ),
                OutlinedButton(
                  style: ButtonStyle(
                      shape: MaterialStateProperty.resolveWith((states) =>
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(14)))),
                  onPressed: () {},
                  child: Text(
                    'Rate Now',
                    style: TextStyle(color: Colors.teal),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
