import 'package:flutter/material.dart';
import 'package:taqqat_user_app_demo/widgets/base_row.dart';
import 'package:taqqat_user_app_demo/widgets/common_material_button.dart';
import 'package:taqqat_user_app_demo/constants.dart';
import 'package:taqqat_user_app_demo/route.dart';

int valueNow = 5500;

class SixthScreen extends StatefulWidget {
  @override
  _SixthScreenState createState() => _SixthScreenState();
}

class _SixthScreenState extends State<SixthScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/Survey-start-img – 3.png'),
              fit: BoxFit.fill,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: IconButton(
                    onPressed: () {
                      print('previous page');
                      Navigator.pushNamed(context, fifthRoute);
                    },
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.grey[600],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Choose your building top area on which the installation planned',
                  style: kTitleTextStyle,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Lorem ipsum dolor sit amet, coorem ipsum dolor sit amet, consect'
                  'nsectetur adipiscing elit,',
                  textAlign: TextAlign.start,
                  style: kDescriptionTextStyle,
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Image.asset(
                  'images/Group 337.png',
                  height: 200,
                ),
              ),
              Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('Top Area'),
                  )),
              SliderTheme(
                data: SliderThemeData(
                  trackHeight: 10,
                  thumbColor: Colors.white,
                  activeTrackColor: Colors.yellow[600],
                  inactiveTrackColor: Colors.grey[50],
                  thumbShape: RoundSliderThumbShape(enabledThumbRadius: 13),
                  //overlayColor: Colors.grey[50],
                ),
                child: Slider(
                  value: valueNow.toDouble(),
                  min: 1000.0,
                  max: 10000.0,
                  divisions: 50,
                  onChanged: (double value) {
                    setState(() {
                      valueNow = value.toInt();
                    });
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  '$valueNow Sqr ft',
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  shadowColor: Colors.grey,
                  child: Text(
                    '$valueNow Sqr ft',
                  ),
                ),
              ),
              baseRow(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  CommonMaterialButton(
                    onPress: () {
                      print('previous page');
                      Navigator.pushNamed(context, fifthRoute);
                    },
                    title: 'Back',
                    colour: Colors.white,
                    fontColour: Colors.teal,
                  ),
                  CommonMaterialButton(
                    onPress: () {},
                    title: 'Next',
                    colour: Colors.teal,
                    fontColour: Colors.white,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
