import 'package:flutter/material.dart';
import 'package:taqqat_user_app_demo/constants.dart';
import 'package:taqqat_user_app_demo/route.dart';

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizedBox buttonFunction(
        {required String printData,
        required String imageData,
        required String titleData}) {
      return SizedBox(
        height: 60,
        child: OutlinedButton(
            style: ButtonStyle(
              shape: MaterialStateProperty.all(RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0))),
            ),
            onPressed: () {
              print('$printData');

              Navigator.of(context).pushNamed(secondRoute);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  '$imageData',
                  // height: 40,
                  //width: 40,
                ),
                Text(
                  '$titleData',
                  style: TextStyle(
                    //fontSize: 15,
                    fontWeight: FontWeight.w400,
                    color: Colors.black,
                  ),
                ),
                Icon(
                  Icons.arrow_forward_ios_rounded,
                  color: Colors.grey,
                )
              ],
            )),
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Expanded(
            flex: 3,
            child: Image.asset(
              'images/Group 16342.png',
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Choose your language',
              style: kTitleTextStyle.copyWith(fontWeight: FontWeight.w600),
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              'Lorem ipsum dolosdfdndh ,sjod jhdeuh ncdbc duedhe churgfffffffffffffffffr sit amet, consectetur adipiscing elit. Nullam non lectus sollicitudin',
              textAlign: TextAlign.center,
              style: kDescriptionTextStyle,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: buttonFunction(
                printData: 'Selected English',
                imageData: 'images/union-jack-flag.png',
                titleData: 'English'),
          ),
          //Positioned(
          //bottom: deviceHeight * .09,
          // left: deviceWidth * .03,
          // right: deviceWidth * .03,
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: buttonFunction(
                printData: 'Selected Arab',
                imageData: 'images/Mask Group 227.png',
                titleData: 'Arab'),
          ),
        ]),
      ),
    );
  }
}
