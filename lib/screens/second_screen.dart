import 'package:flutter/material.dart';
import 'package:taqqat_user_app_demo/constants.dart';
import 'package:taqqat_user_app_demo/route.dart';
import 'package:taqqat_user_app_demo/widgets/common_material_button.dart';

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
          Align(
            alignment: Alignment.topLeft,
            child: IconButton(
              onPressed: () {
                print('back');
                Navigator.of(context).pushNamed(firstRoute);
              },
              icon: Icon(Icons.arrow_back_ios),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Image.asset(
              'images/Group 16225.png',
              height: 360,
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "Sed ut perspiciatis,voluptatem!",
                style: kTitleTextStyle.copyWith(fontWeight: FontWeight.w600),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Lorem ipsum dolorcej jeyi edjoiej ejdeij  sitcing elmnd dnjh djhe it, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad, quis  exercitation 😎😎 ',
              textAlign: TextAlign.center,
              style: kDescriptionTextStyle,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              // crossAxisAlignment: CrossAxisAlignment.baseline,
              children: [
                Align(
                  alignment: Alignment.bottomLeft,
                  child: IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Icons.more_horiz_outlined,
                        color: Colors.teal,
                        size: 20,
                      )),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: CommonMaterialButton(
                    onPress: () {
                      print('clicked');
                      Navigator.of(context).pushNamed(thirdRoute);
                    },
                    colour: Colors.teal,
                    title: 'Get Started',
                    fontColour: Colors.white,
                  ),
                ),
              ],
            ),
          )
        ]),
      ),
    );
  }
}
