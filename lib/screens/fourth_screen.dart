import 'package:flutter/material.dart';
import 'package:taqqat_user_app_demo/constants.dart';
import 'package:taqqat_user_app_demo/route.dart';
import 'package:taqqat_user_app_demo/widgets/common_material_button.dart';

class FourthPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/Survey-start-img – 3.png'),
                fit: BoxFit.fill,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: IconButton(
                      onPressed: () {
                        print('previous page');
                        Navigator.of(context).pushNamed(thirdRoute);
                      },
                      icon: Icon(Icons.arrow_back_ios)),
                ),
                SizedBox(
                  height: 200,
                  width: 250,
                  child: Image.asset(
                    'images/survey-start-img.png',
                    fit: BoxFit.fill,
                  ),
                ),
                Center(
                  child: Text(
                    'Let\’s have a quick survey to know about solar power',
                    textAlign: TextAlign.center,
                    style:
                        kTitleTextStyle.copyWith(fontWeight: FontWeight.w600),
                  ),
                ),
                Text(
                    'Lorem ipsli ndh bdjfbdjf'
                    'um dolorf gfr vdd sit amet, '
                    'consectetur '
                    'um dolorf gfr vdd sit amet, '
                    'consectetur adipiscing elitbdjf'
                    'um dolorf gfr vdd sit amet, '
                    'consectetur adi,',
                    textAlign: TextAlign.center,
                    style: kDescriptionTextStyle),
                CommonMaterialButton(
                  onPress: () {
                    Navigator.of(context).pushNamed(fifthRoute);
                  },
                  title: 'Get Started',
                  colour: Colors.teal,
                  fontColour: Colors.white,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Already have an account?',
                        style: TextStyle(
                          fontSize: 13,
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          print('go to log in page');
                        },
                        child: Text(
                          'log in',
                          style: TextStyle(
                            fontSize: 13,
                            color: Colors.teal[700],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
