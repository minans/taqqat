import 'package:flutter/material.dart';

const kDescriptionTextStyle = TextStyle(
  fontSize: 15,
  color: Colors.grey,
);
const kTitleTextStyle = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold
);
const kHeadingTextStyle = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w600
);
bool validateName(String name){
  if(name.length<3)
    return true;
  else
    return false;
}
bool validatePhone(String phone){
  if(phone.length!=10)return true;
  else return false;
}
bool validateEmail(String email) {
  String pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern);
  if (!regex.hasMatch(email))
    return true;
  else
    return false;
}