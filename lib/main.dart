import 'package:flutter/material.dart';
import 'package:taqqat_user_app_demo/route.dart';
import 'package:taqqat_user_app_demo/screens/first_screen.dart';
import 'package:taqqat_user_app_demo/screens/support_request.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.white,
        accentColor: Colors.teal,
        scaffoldBackgroundColor: Colors.cyan[50],
      ),
      // home: FourthPage(),
      home: SupportRequestClass(),
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}
