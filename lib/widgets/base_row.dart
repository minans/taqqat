import 'package:flutter/material.dart';

Padding baseRow() {
  return Padding(
    padding: const EdgeInsets.all(15.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Icon(Icons.info_outline_rounded),
        SizedBox(
          width: 330,
          child: Text(
            'You have option to check the calculator '
                ' by adjusting the  price later',
            textAlign: TextAlign.center,
          ),
        )
      ],
    ),
  );
}
