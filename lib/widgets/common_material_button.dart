import 'package:flutter/material.dart';

class CommonMaterialButton extends StatelessWidget {
  final VoidCallback onPress;
  final String title;
  final Color colour;
  final Color fontColour;
  CommonMaterialButton(
      {required this.onPress,
        required this.title,
        required this.colour,
        required this.fontColour});
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
        minWidth: 140,
        height: 50,
        color: colour,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        onPressed: onPress,
        child: Text(
          title,
          style: TextStyle(
            fontSize: 15,
            color: fontColour,
          ),
        ));
  }
}
