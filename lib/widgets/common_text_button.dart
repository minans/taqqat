import 'package:flutter/material.dart';

class CommonTextButton extends StatelessWidget {
  final String buttonTitle;
  final TextStyle textDecoration;
  final VoidCallback function;
  CommonTextButton({required this.buttonTitle,required this.textDecoration,required this.function});

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
        overlayColor: MaterialStateProperty.resolveWith((states) => Colors.grey[200]),
      ),
      onPressed: function,
      child: Text(
        buttonTitle,
        style: textDecoration,
      ),
    );
  }
}
