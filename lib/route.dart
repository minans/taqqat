
import 'package:flutter/material.dart';
import 'package:taqqat_user_app_demo/screens/fifth_screen.dart';
import 'package:taqqat_user_app_demo/screens/first_screen.dart';
import 'package:taqqat_user_app_demo/screens/fourth_screen.dart';
import 'package:taqqat_user_app_demo/screens/second_screen.dart';
import 'package:taqqat_user_app_demo/screens/sixth_screen.dart';
import 'package:taqqat_user_app_demo/screens/third_screen.dart';


const String firstRoute='first';
const String secondRoute='second';
const String thirdRoute='third';
const String fourthRoute='fourth';
const String fifthRoute='fifth';
const String sixthRoute='sixth';

class RouteGenerator {

  static   Route<dynamic> generateRoute (RouteSettings settings) {

    switch (settings.name) {
      case firstRoute:
        return MaterialPageRoute(builder: (context) => FirstPage());
      case secondRoute:
        return MaterialPageRoute(builder: (context) => SecondPage());
      case thirdRoute:
        return MaterialPageRoute(builder: (_) => ThirdPage());
      case fourthRoute:
        return MaterialPageRoute(builder: (_) => FourthPage());
      case fifthRoute:
        return MaterialPageRoute(builder: (_) => FifthPage());
      case sixthRoute:
        return MaterialPageRoute(builder: (_) => SixthScreen());
    }
    throw('error');
  }
}
